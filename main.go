package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/pkg/browser"
)

var picoHTTPVersion = "-set-during-build-"

var flagListen = flag.String("listen", "[::]:8000", "Address to listen on. Can also be set via the LISTEN environment variable.")
var flagRoot = flag.String("root", "", "Directory to serve, defaults to the current working directory. Can also be set via the WEBROOT environment variable.")
var flagNoBrowser = flag.Bool("no-browser", false, "By default picohttp opens a webbrowser, use this option to prevent that.")

func main() {
	flag.Parse()

	var listen, webroot string

	if flagListen == nil || *flagListen == "" {
		listen = os.ExpandEnv("${LISTEN}")
	} else {
		listen = *flagListen
	}
	if flagRoot == nil || *flagRoot == "" {
		webroot = os.ExpandEnv("${WEBROOT}")
	} else {
		webroot = *flagRoot
	}
	if webroot == "" {
		var err error
		if webroot, err = os.Getwd(); err != nil {
			panic(err)
		}
	}

	version := fmt.Sprintf("PicoHTTP %s", picoHTTPVersion)
	log.Print("Starting ", version)
	log.Printf("Listening on %v", listen)
	log.Printf("Webroot:     %v", webroot)

	httpServer := &http.Server{
		Addr:         listen,
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	filehandler := http.FileServer(http.Dir(webroot))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Server", version)
		filehandler.ServeHTTP(w, r)
	})

	if ownURL, err := findOwnURL(listen); err != nil {
		fmt.Printf("Warning: could not construct my own URL: %v", err)
		// It's fine to continue. We'll see if the HTTP server has anything to say
		// about this 'listen' parameter.
	} else {
		log.Printf("URL:         %v", ownURL.String())

		if flagNoBrowser == nil || !*flagNoBrowser {
			go func() {
				time.Sleep(250 * time.Millisecond)
				openBrowser(ownURL)
			}()
		}
	}

	log.Fatal(httpServer.ListenAndServe())
}

func findOwnURL(listen string) (url.URL, error) {
	host, port, err := net.SplitHostPort(listen)
	if err != nil {
		return url.URL{}, err
	}

	switch host {
	case "::", "*", "":
		host = "localhost"
	}

	ownURL := url.URL{
		Scheme: "http",
		Host:   net.JoinHostPort(host, port),
		Path:   "/",
	}

	return ownURL, nil
}

func openBrowser(ownURL url.URL) {
	err := browser.OpenURL(ownURL.String())

	if err != nil {
		log.Printf(
			"unable to open a browser to %s, please open it yourself or try any of the other URLs above: %v",
			ownURL.String(), err)
		return
	}
}
