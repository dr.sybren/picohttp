OUT := picohttp
PKG := gitlab.com/dr.sybren/picohttp
VERSION := $(shell git describe --tags --dirty --always)
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)

LDFLAGS := ${LDFLAGS} -X main.picoHTTPVersion=${VERSION}
BUILD_FLAGS = -ldflags="${LDFLAGS}"

all: vet server

server:
	CGO_ENABLED=0 go build -v -o ${OUT} ${BUILD_FLAGS} ${PKG}

install:
	CGO_ENABLED=0 go install -v ${BUILD_FLAGS} ${PKG}

version:
	@echo "Package: ${PKG}"
	@echo "Version: ${VERSION}"

test:
	go test -short ${PKG_LIST}

vet:
	go vet ${PKG_LIST}

run: server
	./${OUT}

clean:
	@go clean -i -x
	rm -f ${OUT}-v*

.PHONY: run server version vet
