# PicoHTTP

This is a tiny static HTTP server. It is intended to be run locally to serve
files, primarily [RevealJS presentations](https://revealjs.com/).

The server can be configured using environment variables or CLI arguments:

| Environment | CLI           | Meaning                                                   |
|-------------|---------------|-----------------------------------------------------------|
| `WEBROOT`   | `-root`       | The filesystem path to serve.                             |
| `LISTEN`    | `-listen`     | The TCP/IP address to listen on, in "host:port" notation. |
| `-`         | `-no-browser` | Do not open a webbrowser to see the served files.         |

## Building

- Install [Go](https://go.dev/).
- Run some environment that includes `make`.
- Run `make install` to locally build & install PicoHTTP.

## Changelog

### Version 1.3 (2024-05-09)

- Open webbrowser automatically, unless `-no-browser` is passed on the CLI.
- Bump Go version to the latest (1.22.3) for security updates and stuff.

### Version 1.2 (2022-09-27)

- Add CLI flags `-listen` and `-root`

### Version 1.1 (2019-07-10)

- Avoid building via Docker, use Makefile instead.

### Version 1.0 (2018-02-13)

- Initial version.
- Development started 2017-07-07
